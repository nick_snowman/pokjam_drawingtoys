using System.Collections.Generic;
using Common;
using DG.Tweening;
using UnityEngine;
using UnityEngine.U2D;

namespace ConnectTheDots
{
    public class Fill : MonoBehaviour
    {
        private const float SCALE_UP_TIME = 75f;

        public static event System.Action<Fill> OnNewFillInitialized;
        
        private Tween tween;

        private SpriteShapeRenderer spriteShapeRenderer;
        private SpriteMask spriteMask;

        private void Awake()
        {
            OnNewFillInitialized += HandleNewFillInitialized;
        }

        private void HandleNewFillInitialized(Fill fill)
        {
            if (fill == this) return;
            
            if (spriteShapeRenderer == null)
                spriteShapeRenderer = GetComponent<SpriteShapeRenderer>();
            
            if (spriteMask == null)
                spriteMask = GetComponentInChildren<SpriteMask>();

            spriteShapeRenderer.maskInteraction = SpriteMaskInteraction.None;
            spriteMask.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            if (tween != null)
            {
                tween.Kill();
            }

            OnNewFillInitialized -= HandleNewFillInitialized;
        }

        public void InitializeFill(List<Dot> loopDots, Color color)
        {
            OnNewFillInitialized?.Invoke(this);
            
            // Play sound.
            SoundManager.PlayOneShot("colorFillRR");
            
            // Set self on correct "draw layer" for fills.
            transform.position = new Vector3(transform.position.x, transform.position.y, (float) DrawLayer.Fill);

            // Set mask interaction.
            spriteShapeRenderer = GetComponent<SpriteShapeRenderer>();
            spriteShapeRenderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            
            // Set up spline shape.
            SpriteShapeController spriteShapeController = GetComponent<SpriteShapeController>();
            Spline spline = spriteShapeController.spline;
            spline.Clear();
            Vector3 avgPointPosition = Vector3.zero;

            for (int i = 0; i < loopDots.Count; i++)
            {
                Vector3 point = loopDots[i].PositionFillLayer;
                spline.InsertPointAt(i, point);
                spline.SetTangentMode(i, ShapeTangentMode.Linear);
                spline.SetHeight(i, 0);
                spline.SetCorner(i, false);

                avgPointPosition += point;
            }

            // Get the average position of all points (for figuring out where the fill center will appear)
            avgPointPosition = new Vector3(
                avgPointPosition.x / loopDots.Count, 
                avgPointPosition.y / loopDots.Count,
                (float) DrawLayer.Fill);
            
            // spriteShapeController.RefreshSpriteShape();
            // spriteShapeController.BakeCollider();
            
            // Get the furthest distance between two points in the loop to scale fill time
            // This is O(n^2) which can be improved but meh it's a jam
            // float maxDist = float.NegativeInfinity;
            // foreach (Dot dot in loopDots)
            // {
            //     foreach (Dot compareDot in loopDots)
            //     {
            //         float dist = Vector2.Distance(dot.PositionDotLayer, compareDot.PositionDotLayer);
            //         if (dist > maxDist)
            //         {
            //             maxDist = dist;
            //         }
            //     }
            // }


            // Set colour.
            spriteShapeRenderer.color = color;
            
            // Set scaling animation.
            spriteMask = GetComponentInChildren<SpriteMask>();
            spriteMask.transform.position = avgPointPosition;
            Vector3 zeroXY = new Vector3(0, 0, 1);
            spriteMask.transform.localScale = zeroXY;

            tween = spriteMask.transform.DOScale(new Vector3(100, 100, 0), SCALE_UP_TIME) // * maxDist)
                .SetEase(Ease.OutCubic)
                .OnComplete(() =>
                {
                    spriteShapeRenderer.maskInteraction = SpriteMaskInteraction.None;
                    spriteMask.gameObject.SetActive(false);
                });
        }
    }
}