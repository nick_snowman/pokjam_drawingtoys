using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace ConnectTheDots
{
    [RequireComponent(typeof(CircleCollider2D))]
    public class Dot : MonoBehaviour
    {
        public event System.Action<Dot> OnDotClicked;

        [SerializeField]
        private CircleCollider2D circleCollider;
        
        [SerializeField]
        private SpriteRenderer spriteRenderer;

        [SerializeField]
        private SpriteRenderer selectionCircle;

        // Set by the grid manager.
        [SerializeField]
        private int row;
        [SerializeField]
        private int col;

        private List<Dot> adjacencies = new List<Dot>();
        public List<Dot> Adjacencies => adjacencies;

        public int Row => row;
        public int Col => col;
        public Vector3 PositionDotLayer => new Vector3(transform.position.x, transform.position.y, (float) DrawLayer.Dot);
        public Vector3 PositionLineLayer => new Vector3(transform.position.x, transform.position.y, (float) DrawLayer.Line);
        public Vector3 PositionFillLayer => new Vector3(transform.position.x, transform.position.y, (float) DrawLayer.Fill);

        private Sequence selectionSequence = null;
        
        private void OnValidate()
        {
            if (circleCollider == null)
            {
                circleCollider = GetComponent<CircleCollider2D>();
            }
        }

        private void OnMouseOver()
        {
            if (Input.GetMouseButton(0))
            {
                OnDotClicked?.Invoke(this);
            }
        }

        public void EnableCollider()
        {
            circleCollider.enabled = true;
        }

        public void DisableCollider()
        {
            circleCollider.enabled = false;
        }

        public void SetSprite(Sprite sprite)
        {
            spriteRenderer.sprite = sprite;
        }

        public void SetRow(int num)
        {
            row = num;
        }
        
        public void SetCol(int num)
        {
            col = num;
        }

        public void AddAdjacency(Dot adjacentDot)
        {
            if (adjacentDot != this && !adjacencies.Contains(adjacentDot) &&
                Mathf.Abs(adjacentDot.Row - this.Row) <= 1 && Mathf.Abs(adjacentDot.Col - this.Col) <= 1)
            {
                adjacencies.Add(adjacentDot);
            }
        }

        public void ClearAdjacencies()
        {
            adjacencies.Clear();
        }

        public void ShowSelectionCircle()
        {
            selectionCircle.gameObject.SetActive(true);
            selectionCircle.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            
            selectionCircle.DOFade(0.5f, 0.15f).From(0f)
                .OnComplete(() =>
                {
                    if (selectionSequence != null)
                    {
                        selectionSequence.Kill();
                    }
                    
                    selectionSequence = DOTween.Sequence()
                        .Append(selectionCircle.transform.DOScale(0.6f, .5f))
                        .Append(selectionCircle.transform.DOScale(0.4f, .5f))
                        .SetLoops(-1);
                });
        }

        public void HideSelectionCircle()
        {
            if (selectionSequence != null)
            {
                selectionSequence.Kill();
            }
            
            selectionCircle.gameObject.SetActive(false);
        }
    }
}