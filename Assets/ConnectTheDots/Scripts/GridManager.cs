using System;
using System.Collections;
using System.Collections.Generic;
using Spin.Scripts.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ConnectTheDots
{
    public enum DrawLayer
    {
        Fill = 0,
        Line = -10,
        Dot = -20
    }
    
    public class GridManager : MonoBehaviour
    {
        [SerializeField]
        private ConnectTheDotsData data;
        
        [Header("Element Parent Transforms")]
        [SerializeField]
        private Transform fillsParentWorldSpace;
        
        [SerializeField]
        private Transform linesParentWorldSpace;
        
        [SerializeField]
        private Transform dotGridParentWorldSpace;

        [Header("Layout")]
        [SerializeField]
        private GameObject dotPlacementCanvasGameObject;
        
        [SerializeField]
        private Transform dotPlacementGridLayoutTransform;

        [Header("Prefabs")]
        [SerializeField]
        private GameObject dotPrefab;

        [SerializeField]
        private GameObject linePrefab;

        [SerializeField]
        private GameObject fillPrefab;

        [Header("Colors")]
        [SerializeField]
        private Color[] colors;
        
        // 2D list where dots[row][col]
        private List<List<Dot>> dots = null;

        /// <summary>
        /// True when a first/starting dot has been activated and a line can be drawn from that dot to another.
        /// </summary>
        private Dot activeFirstDot = null;

        /// <summary>
        /// List of all drawn lines on the grid.
        /// </summary>
        private List<Line> lines = new List<Line>();
        
        /// <summary>
        /// List of all drawn fills on the grid.
        /// </summary>
        private List<Fill> fills = new List<Fill>();

        // Color.clear is considered rainbow color
        private Color activeColor = Color.black;

        private void Awake()
        {
            ColorButton.OnColorButtonPressed += HandleColorButtonPressed;
            ClearButton.OnClearButtonPressed += HandleClearButtonPressed;
        }

        private void Start()
        {
            StartCoroutine(InitializeDotGrid());
        }

        private void OnDestroy()
        {
            ColorButton.OnColorButtonPressed -= HandleColorButtonPressed;
            ClearButton.OnClearButtonPressed -= HandleClearButtonPressed;

            foreach (List<Dot> row in dots)
            {
                foreach (Dot dot in row)
                {
                    if (dot != null)
                    {
                        dot.OnDotClicked -= HandleDotClicked;
                    }
                }
            }
        }

        private void HandleColorButtonPressed(ColorButton colorButton)
        {
            activeColor = colorButton.IsRainbowColor ? Color.clear : colorButton.Color;
        }
        
        private void HandleClearButtonPressed(ClearButton clearButton)
        {
            // Clear all adjacencies
            foreach (List<Dot> row in dots)
            {
                foreach (Dot dot in row)
                {
                    if (dot != null)
                        dot.ClearAdjacencies();
                }
            }
            
            // Remove all lines
            Line[] linesArray = lines.ToArray();
            foreach (Line line in linesArray)
            {
                Destroy(line.gameObject);
            }
            lines.Clear();

            // Remove all fills
            Fill[] fillsArray = fills.ToArray();
            foreach (Fill fill in fillsArray)
            {
                Destroy(fill.gameObject);
            }
            fills.Clear();
        }

        private IEnumerator InitializeDotGrid()
        {
            // Wait a frame for grid layout to set up.
            yield return null;

            // Create the 2D list with row 0 ready to go.
            dots = new List<List<Dot>> {new List<Dot>()};
            int row = 0;
            int col = 0;
            float topLeftX = 0;
            float currentY = 0;
            
            foreach (Transform child in dotPlacementGridLayoutTransform)
            {
                GameObject dotGO = Instantiate(dotPrefab, child.TransformPoint(child.position), Quaternion.identity, dotGridParentWorldSpace);
                ConnectTheDotsUtilities.SetToDrawLayer(dotGO, DrawLayer.Dot);

                Dot dot = dotGO.GetComponent<Dot>();
                dot.SetSprite(data.RandomDotSprite);
                dot.OnDotClicked += HandleDotClicked;

                if (child == dotPlacementGridLayoutTransform.GetChild(0))
                {
                    Vector2 topLeftGridPosition = dot.PositionDotLayer;
                    topLeftX = topLeftGridPosition.x;
                    currentY = topLeftGridPosition.y;
                }

                Vector2 dotPosition = dot.transform.position;

                // Reset column count when reached starting column again.
                if (Math.Abs(dotPosition.x - topLeftX) < ConnectTheDotsUtilities.FLOAT_EQUAL_EPSILON)
                {
                    col = 0;
                }
                dot.SetCol(col);
                col++;

                // Increment row.
                if (Math.Abs(dotPosition.y - currentY) > ConnectTheDotsUtilities.FLOAT_EQUAL_EPSILON)
                {
                    row++;
                    currentY = dotPosition.y;
                    dots.Add(new List<Dot>());
                }
                dot.SetRow(row);
                
                dots[row].Add(dot);
            }
            
            dotPlacementCanvasGameObject.SetActive(false);
        }
        
        private void HandleDotClicked(Dot clickedDot)
        {
            if (activeFirstDot == null)
            {
                clickedDot.DisableCollider();
                activeFirstDot = clickedDot;
                
                StartCoroutine(DrawLineFromDotToMouse(clickedDot));
            }
            else if (LineIsValid(activeFirstDot, clickedDot))
            {
                // Easier naming of clicked dots.
                Dot startDot = activeFirstDot;
                Dot endDot = clickedDot;
                
                activeFirstDot.EnableCollider();
                activeFirstDot = null;
                
                DrawFixedLine(startDot, endDot);

                // Add dots to each adjacency list, and update dots between the two points
                // if they have been intersected.
                UpdateAdjacenciesBetweenDots(startDot, endDot);

                // Draw a fill inside the cycle if it exists.
                DrawFillIfCycleExists(startDot, endDot);
            }
        }

        private void UpdateAdjacenciesBetweenDots(Dot dot1, Dot dot2)
        {
            int rowDir = dot2.Row - dot1.Row == 0 ? 0 : Math.Sign(dot2.Row - dot1.Row);
            int colDir = dot2.Col - dot1.Col == 0 ? 0 : Math.Sign(dot2.Col - dot1.Col);
            
            int row = dot1.Row;
            int col = dot1.Col;

            while (row != dot2.Row || col != dot2.Col)
            {
                dots[row][col].AddAdjacency(dots[row + rowDir][col + colDir]);
                dots[row + rowDir][col + colDir].AddAdjacency(dots[row][col]);
                col += colDir;
                row += rowDir;
            }
        }

        private IEnumerator DrawLineFromDotToMouse(Dot dot)
        {
            GameObject lineGO = Instantiate(linePrefab, new Vector3(0, 0, 0), Quaternion.identity, linesParentWorldSpace);            
            Line line = lineGO.GetComponent<Line>();

            ShowValidDotHighlightsFrom(dot);

            while (Input.GetMouseButton(0) && activeFirstDot != null)
            {
                line.SetLineEndpoints(dot, Camera.main.ScreenToWorldPoint(Input.mousePosition));
                yield return null;
            }

            dot.EnableCollider();
            HideValidDotHighlights();
            
            Destroy(lineGO);
            activeFirstDot = null;
        }

        private void ShowValidDotHighlightsFrom(Dot fromDot)
        {
            foreach (List<Dot> row in dots)
            {
                foreach (Dot toDot in row)
                {
                    if (LineIsValid(fromDot, toDot))
                    {
                        toDot.ShowSelectionCircle();
                    }
                }
            }
        }

        private void HideValidDotHighlights()
        {
            foreach (List<Dot> row in dots)
            {
                foreach (Dot dot in row)
                {
                    dot.HideSelectionCircle();
                }
            }
        }

        private void DrawFixedLine(Dot dot1, Dot dot2)
        {
            // Instantiate a line renderer between the two dots.
            GameObject lineGO = Instantiate(linePrefab, new Vector3(0, 0, 0), Quaternion.identity, linesParentWorldSpace);
            
            Line line = lineGO.GetComponent<Line>();
            line.SetLineEndpoints(dot1, dot2);
            ConnectTheDotsUtilities.SetToDrawLayer(lineGO, DrawLayer.Line);

            if (!lines.Contains(line))
            {
                lines.Add(line);
            }
        }
        
        private void DrawFillIfCycleExists(Dot dot1, Dot dot2)
        {
            HashSet<Dot> discoveredDots = new HashSet<Dot>();
            List<Dot> loopDots = new List<Dot>();
            loopDots.Add(dot1);
            
            if (FindCycleRecursive(dot1, dot1, dot2, discoveredDots, loopDots))
            {
                // Instantiate a line renderer between the two dots.                                                                   
                GameObject fillGO = Instantiate(fillPrefab, new Vector3(0, 0, 0), Quaternion.identity, fillsParentWorldSpace);
                Fill fill = fillGO.GetComponent<Fill>();
                Color color = activeColor == Color.clear ? colors[Random.Range(0, colors.Length)] : activeColor;
                
                fill.InitializeFill(loopDots, color);  
                
                if (!fills.Contains(fill))
                {
                    fills.Add(fill);
                }
            }
        }

        private bool FindCycleRecursive(Dot goalDot, Dot parentDot, Dot currentDot, HashSet<Dot> discoveredDots, List<Dot> loopDots)
        {
            discoveredDots.Add(currentDot);

            foreach (Dot adjDot in currentDot.Adjacencies)
            {
                // Skip adjacent dot if you just came from it, or if it has already been discovered in this traversal.
                if (adjDot == parentDot || discoveredDots.Contains(adjDot))
                {
                    continue;
                }

                // If adjacent dot isn't the one you just came from and it's the goal dot, return true.
                // Any other dot, search its adjacencies.
                if (adjDot == goalDot || FindCycleRecursive(goalDot, currentDot, adjDot, discoveredDots, loopDots))
                {
                    if (!loopDots.Contains(currentDot))
                        loopDots.Add(currentDot);
                    return true;
                }
            }

            // No loop was found between current dot and goal dot.
            return false;
        }

        private Dot GetClosestDotToPoint(Vector2 point)
        {
            // Vector2 mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //
            // Dot closestDot = GetClosestDotToPoint(mouseWorldPoint);
            // if (closestDot == null)
            // {
            //     return;
            // }
            
            float minDistance = float.PositiveInfinity;
            Dot closestDot = null;

            foreach (List<Dot> row in dots)
            {
                foreach (Dot dot in row)
                {
                    float distance = Vector2.Distance(point, dot.transform.position);

                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        closestDot = dot;
                    }
                }
            }

            return closestDot;
        }

        /// <summary>
        /// Checks if a line can be drawn between the given start and end dots.
        /// Conditions:
        /// - Cannot draw a line between a dot and itself.
        /// - Cannot draw a line that isn't straight or directly diagonal (slope of 1).
        /// </summary>
        /// <param name="start">The starting dot for the line.</param>
        /// <param name="end">The ending dot for the line.</param>
        /// <returns>True if a line can be drawn between the two dots, false if not.</returns>
        private bool LineIsValid(Dot start, Dot end)
        {
            bool startDoesNotEqualEnd = start != end;
            bool rowsAreEqual = start.Row == end.Row;
            bool colsAreEqual = start.Col == end.Col;
            bool onDirectDiagonal = Mathf.Abs(end.Row - start.Row) == Mathf.Abs(end.Col - start.Col);

            // The order of this conditional is specific and intentional for performance!
            if (startDoesNotEqualEnd &&
                (rowsAreEqual || colsAreEqual || onDirectDiagonal) &&
                !LineExistsBetween(start, end))
            {
                return true;
            }

            return false;
        }

        private bool LineExistsBetween(Dot dot1, Dot dot2)
        {
            int rowDir = dot2.Row - dot1.Row == 0 ? 0 : Math.Sign(dot2.Row - dot1.Row);
            int colDir = dot2.Col - dot1.Col == 0 ? 0 : Math.Sign(dot2.Col - dot1.Col);
            
            int row = dot1.Row;
            int col = dot1.Col;

            while (row != dot2.Row || col != dot2.Col)
            {
                if (!dots[row][col].Adjacencies.Contains(dots[row + rowDir][col + colDir]))
                    return false;
                
                if (!dots[row + rowDir][col + colDir].Adjacencies.Contains(dots[row][col]))
                    return false;
                
                col += colDir;
                row += rowDir;
            }

            return true;
        }
    }
}