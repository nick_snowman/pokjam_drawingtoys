using UnityEngine;

[CreateAssetMenu]
public class ConnectTheDotsData : ScriptableObject
{
    [SerializeField] private Sprite[] dotSprites;
    public Sprite RandomDotSprite => dotSprites[Random.Range(0, dotSprites.Length - 1)];
}
