using UnityEngine;

namespace ConnectTheDots
{
    public static class ConnectTheDotsUtilities
    {
        public const float FLOAT_EQUAL_EPSILON = 0.001f;

        public static void SetToDrawLayer(GameObject go, DrawLayer drawLayer)
        {
            go.transform.position = new Vector3(go.transform.position.x, go.transform.position.y, (float) drawLayer);
        }
    }
}