using UnityEngine;

namespace ConnectTheDots
{
    [RequireComponent(typeof(LineRenderer))]
    public class Line : MonoBehaviour
    {
        private LineRenderer lineRenderer;

        public void SetLineEndpoints(Dot dot0, Dot dot1)
        {
            if (lineRenderer == null)
            {
                lineRenderer = GetComponent<LineRenderer>();
            }
            
            lineRenderer.SetPosition(0, dot0.PositionLineLayer);
            lineRenderer.SetPosition(1, dot1.PositionLineLayer);
        }
        
        public void SetLineEndpoints(Dot dot0, Vector3 endPos)
        {
            if (lineRenderer == null)
            {
                lineRenderer = GetComponent<LineRenderer>();
            }
            
            lineRenderer.SetPosition(0, dot0.PositionLineLayer);
            lineRenderer.SetPosition(1, new Vector3(endPos.x, endPos.y, (float) DrawLayer.Line));
        }
    }
}