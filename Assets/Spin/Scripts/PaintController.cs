﻿using System;
using Spin.Scripts.UI;
using Spin.Scripts.Utilities;
using UnityEngine;

namespace Spin.Scripts
{
    public class PaintController : Singleton<PaintController>
    {
        public static event Action<PaintController, BrushPreset> OnBrushChanged; 
        [SerializeField]
        private BrushPreset currentBrushPreset;
        public BrushPreset CurrentBrushPreset => currentBrushPreset;
        
        private bool initialized;
        [SerializeField]
        private BrushTool currentBrush;
        [SerializeField]
        private Color currentColor;
   
        private Vector3 offset;

        protected override void Awake()
        {
            base.Awake();
            ColorButton.OnColorButtonPressed += HandleColorButtonPressed;
        }

        private void OnDestroy()
        {
            ColorButton.OnColorButtonPressed -= HandleColorButtonPressed;
        }

        private void HandleColorButtonPressed(ColorButton colorButton)
        {
            currentColor = colorButton.Color;
            UpdateCurrentBrushColor();
        }

        private void UpdateCurrentBrushColor()
        {
            if (currentBrushPreset != null)
            {
                currentBrushPreset.SetColor(currentColor);
            }
        }

        public void SetBrush(BrushTool tool)
        {
            if(currentBrush == tool)
                return;

            if (currentBrush != null)
            {
                currentBrush.Unselect();
            }

            currentBrush = tool;
            currentBrushPreset = null;
            if (currentBrush != null)
            {
                currentBrush.Select();
                currentBrushPreset = currentBrush.BrushPreset;
                UpdateCurrentBrushColor();
            }
            
            OnBrushChanged?.Invoke(this, currentBrushPreset);
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(1))
            {
                SetBrush(null);
            }
        }
    }
}