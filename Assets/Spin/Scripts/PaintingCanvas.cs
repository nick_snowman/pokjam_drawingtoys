﻿using DG.Tweening;
using Spin.Scripts.UI;
using Spin.Scripts.Utilities;
using UnityEngine;

namespace Spin.Scripts
{
    public class PaintingCanvas : Singleton<PaintingCanvas>
    {
        [Header("Texture")]
        [SerializeField]
        private Texture2D originalTexture;
        [SerializeField]
        private SpriteRenderer canvas;

        [Header("Rotation")]
        [SerializeField]
        private float speedTweenDuration = 1;
        [SerializeField]
        private Ease speedTweenEase;
        
        [SerializeField]
        private Camera mainCamera;
        
        private float desiredSpeed = 0;
        private float currentSpeed;
        private Tween speedChangingTween;
        private Texture2D currentTexture;
        private Texture2D paintTexture;
        private Sprite currentSprite;
        private bool insideCanvas;
        private bool isPainting;
        private bool mouseWasPreviouslyHeldDown = false;
        private bool noDrawingOnCurrentDrag = false;
        private Vector2 previousDragPosition;
        private Color32[] curColors;
        private BrushPreset currentBrushPreset;
        private Vector2Int currentBrushSize;
        private bool[,] cacheBrush;
        private int pixelY;
        private int pixelX;
        private Vector2 mousePosition;
        private Vector2 pixelPos;
        private float pixelWidth;
        private float pixelHeight;
        private float unitsToPixels;
        private float centeredX;
        private float centeredY;
        private int arrayPos;
        private float distance;
        private float lerpSteps;


        private void OnValidate()
        {
            if (mainCamera == null)
            {
                mainCamera = Camera.main;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            PaintController.OnBrushChanged += HandleBrushChanged;
            ClearButton.OnClearButtonPressed += HandleClearButtonPressed;
            ClearCanvas();
        }

        private void ClearCanvas()
        {
            currentTexture = CreateClearTexture();
            currentSprite = Sprite.Create(currentTexture, new Rect(0.0f, 0.0f, currentTexture.width, currentTexture.height), new Vector2(0.5f, 0.5f), 70.0f);
            canvas.sprite = currentSprite;
        }

        private void OnDestroy()
        {
            PaintController.OnBrushChanged -= HandleBrushChanged;
            ClearButton.OnClearButtonPressed -= HandleClearButtonPressed;
            
            speedChangingTween?.Kill();
        }

        private void HandleBrushChanged(PaintController paintController, BrushPreset brushPreset)
        {
            currentBrushPreset = brushPreset;
            currentBrushSize = new Vector2Int(currentBrushPreset.SourceTexture.width, currentBrushPreset.SourceTexture.height);
            Color[] pixels = currentBrushPreset.SourceTexture.GetPixels();
            cacheBrush = new bool[currentBrushSize.x, currentBrushSize.y];

            for (int y = 0; y < currentBrushSize.x; y++)
            {
                for (int x = 0; x <  currentBrushSize.y; x++)
                {
                    arrayPos = y * currentBrushSize.x + x;
                    cacheBrush[x, y] = pixels[arrayPos].Equals(Color.white);
                }
            }
        }

        private Texture2D CreateClearTexture()
        {
            Texture2D texture = new Texture2D(originalTexture.width, originalTexture.height);
            texture.SetPixels(originalTexture.GetPixels());
            texture.Apply();
            return texture;
        }

        public void ChangeSpeed(float speed)
        {
            desiredSpeed = speed;
            speedChangingTween?.Kill();
            speedChangingTween = DOTween.To(x => currentSpeed = x, currentSpeed, desiredSpeed, speedTweenDuration)
                .SetEase(speedTweenEase);
        }
        
        private void Update()
        {
            if (currentSpeed > 0)
            {
                transform.Rotate(0,0,currentSpeed * Time.deltaTime);
            }
            
            if (isPainting)
            {
                Vector3 originalMousePosition = Input.mousePosition;
                originalMousePosition.z = 10;
                mousePosition = transform.InverseTransformPoint(mainCamera.ScreenToWorldPoint(originalMousePosition));

                if (insideCanvas && !noDrawingOnCurrentDrag)
                {
                    DrawWithBrush(mousePosition);
                }
                else if(!insideCanvas)
                {
                    previousDragPosition = Vector2.zero;
                    if (!mouseWasPreviouslyHeldDown)
                    {
                        noDrawingOnCurrentDrag = true;
                    }
                }
            }
            else
            {
                previousDragPosition = Vector2.zero;
                noDrawingOnCurrentDrag = false;
            }
            mouseWasPreviouslyHeldDown = isPainting;
        }

        private void DrawWithBrush(Vector2 worldPoint)
        {
            if(currentBrushPreset == null)
                return;
            
            pixelPos = LocalToPixelCoordinates(worldPoint);

            curColors = currentTexture.GetPixels32();

            if (previousDragPosition == Vector2.zero)
            {
                MarkPixelsToColour(pixelPos, currentBrushPreset.SourceTexture, currentBrushPreset.Color);
            }
            else
            {
                ColourBetween(previousDragPosition, pixelPos, currentBrushPreset.SourceTexture, currentBrushPreset.Color);
            }
            ApplyMarkedPixelChanges();

           previousDragPosition = pixelPos;
        }
        
        private Vector2 LocalToPixelCoordinates(Vector2 localPos)
        {
            pixelWidth = currentSprite.rect.width;
            pixelHeight = currentSprite.rect.height;
            unitsToPixels = pixelWidth / currentSprite.bounds.size.x * transform.localScale.x;
            
            centeredX = localPos.x * unitsToPixels + pixelWidth / 2;
            centeredY = localPos.y * unitsToPixels + pixelHeight / 2;

            return new Vector2(Mathf.RoundToInt(centeredX), Mathf.RoundToInt(centeredY));
        }

        private void MarkPixelsToColour(Vector2 centerPixel, Texture2D brushShape, Color colorOfPen)
        {
            for (int y = 0; y < brushShape.height; y++)
            {
                pixelY = (int)centerPixel.y + y - brushShape.height / 2;
                if(pixelY < 0 || pixelY > (int)currentSprite.rect.height)
                    continue;
                
                for (int x = 0; x < brushShape.width; x++)
                {
                    pixelX = (int)centerPixel.x + x - brushShape.width / 2;
                    if(pixelX < 0 || pixelX > (int)currentSprite.rect.width)
                        continue;

                    if (cacheBrush[x,y])
                    {
                        MarkPixelToChange(pixelX, pixelY, colorOfPen);
                    }
                }
            }
        }
        
        private void MarkPixelToChange(int x, int y, Color color)
        {
            arrayPos = y * (int)currentSprite.rect.width + x;
            
            if (arrayPos > curColors.Length - 1 || arrayPos < 0)
                return;
            
            curColors[arrayPos] = color;
        }

        private void ColourBetween(Vector2 startPoint, Vector2 endPoint, Texture2D brushTexture, Color color)
        {
            distance = Vector2.Distance(startPoint, endPoint);
            lerpSteps = 1 / distance;

            for (float lerp = 0; lerp <= 1; lerp += lerpSteps)
            {
                MarkPixelsToColour(Vector2.Lerp(startPoint, endPoint, lerp), brushTexture, color);
            }
        }

        private void ApplyMarkedPixelChanges()
        {
            currentTexture.SetPixels32(curColors);
            currentTexture.Apply();
        }
        
        private void OnMouseDown()
        {
            isPainting = true;
        }

        private void OnMouseUp()
        {
            isPainting = false;
        }

        private void OnMouseExit()
        {
            insideCanvas = false;
        }

        private void OnMouseEnter()
        {
            insideCanvas = true;
        }
        
        private void HandleClearButtonPressed(ClearButton btn)
        {
            ClearCanvas();
        }
    }
}