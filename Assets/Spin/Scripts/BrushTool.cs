﻿using Common;
using DG.Tweening;
using UnityEngine;

namespace Spin.Scripts
{
    public class BrushTool : MonoBehaviour
    {
        [SerializeField]
        private BrushPreset brushPreset;
        public BrushPreset BrushPreset => brushPreset;

        [Header("Tween")]
        [SerializeField]
        private float brushShakeDuration = 1.5f;
        [Min(0)]
        [SerializeField]
        private float shakeStrength;
        [Min(0)]
        [SerializeField]
        private int shakeVibrato;
        [SerializeField]
        private Ease shakeEase;

        [SerializeField]
        private SpriteRenderer spriteRenderer;

        private Tween tween;
        private bool isSelected;

        private void OnDestroy()
        {
            tween?.Kill();
        }

        private void OnMouseDown()
        {
            if(PaintController.Instance != null)
                PaintController.Instance.SetBrush(this);
        }

        public void Select()
        {
            tween?.Kill();
            tween = DOTween.Sequence()
                .Append(transform.DOShakeRotation(brushShakeDuration, shakeStrength, shakeVibrato, 0).SetEase(shakeEase))
                .Append(spriteRenderer.DOFade(0, 0.5f).SetEase(Ease.Linear));
            SoundManager.PlayOneShot("brushSelect");
        }

        public void Unselect()
        {
            tween?.Kill();
            tween = DOTween.Sequence()
                .Append(spriteRenderer.DOFade(1, 0.5f).SetEase(Ease.Linear))
                .Append(transform.DOShakeRotation(brushShakeDuration, shakeStrength, shakeVibrato, 0)
                    .SetEase(shakeEase));

        }
    }
}