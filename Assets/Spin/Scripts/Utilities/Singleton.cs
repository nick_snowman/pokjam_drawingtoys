﻿using UnityEngine;

namespace Spin.Scripts.Utilities
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance { get; private set; }

        protected virtual void Awake()
        {
            CacheInstance();
        }

        private void CacheInstance()
        {
            if (Instance == null)
            {
                Instance = GetComponent<T>();
            }
            else
            {
                string type = typeof(T).ToString();
                Debug.LogWarning("Singleton<" + type + "> instance created already.");
                DestroyImmediate(this);
            }
        }
    }
}