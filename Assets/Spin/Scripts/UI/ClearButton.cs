﻿using System;
using Common;
using DG.Tweening;
using UnityEngine;

namespace Spin.Scripts.UI
{
    public class ClearButton : BaseButton
    {
        public static event Action<ClearButton> OnClearButtonPressed;

        [Header("Move tween")]
        [SerializeField]
        private float duration;
        [SerializeField]
        private float slidePositionX;
        [SerializeField]
        private Ease slideEase;

        private float originalPositionX;

        [SerializeField]
        private RectTransform rectTransform;

        protected override void OnValidate()
        {
            base.OnValidate();

            if (rectTransform == null)
            {
                rectTransform = GetComponent<RectTransform>();
            }
        }

        private void Start()
        {
            originalPositionX = transform.localPosition.x;
        }

        protected override void HandleButtonClick()
        {
            SoundManager.PlayOneShot("clearCanvas");

            button.interactable = false;
            tween?.Kill();
            tween = DOTween.Sequence()
                .Append(rectTransform.DOLocalMoveX(slidePositionX, duration).SetEase(slideEase))
                .Append(rectTransform.DOLocalMoveX(originalPositionX, duration).SetEase(slideEase))
                .OnComplete(() => button.interactable = true);
            
            OnClearButtonPressed?.Invoke(this);
        }
    }
}