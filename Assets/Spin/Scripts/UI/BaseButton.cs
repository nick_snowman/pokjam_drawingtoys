﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Spin.Scripts.UI
{
    [RequireComponent(typeof(Button))]
    public abstract class BaseButton : MonoBehaviour
    {
        [SerializeField]
        private bool tweenInteract;
        [SerializeField]
        private float shakeDuration;
        [SerializeField]
        private float shakeStrength;
        [SerializeField]
        private int shakeVibrato;
        [SerializeField]
        private int randomness;
        [SerializeField]
        private Ease shakeEase;

        [HideInInspector][SerializeField]
        protected Button button;

        protected Tween tween;

        protected virtual void OnValidate()
        {
            if (button == null)
            {
                button = GetComponent<Button>();
            }
        }

        protected virtual void Awake()
        {
            button.onClick.AddListener(HandleButtonClick);
        }

        protected virtual void OnDestroy()
        {
            tween?.Kill();
            
            if (button != null)
            {
                button.onClick.RemoveListener(HandleButtonClick);
            }
        }

        protected virtual void HandleButtonClick()
        {
            if (tweenInteract)
            {
                tween?.Kill();
                tween = transform.DOShakePosition(shakeDuration, shakeStrength, shakeVibrato, randomness)
                    .SetEase(shakeEase);
            }
        }
    }
}