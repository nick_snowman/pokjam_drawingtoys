using System;
using Common;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Spin.Scripts.UI
{
    [RequireComponent(typeof(Image))]
    public class ColorButton : BaseButton
    {
        public static event Action<ColorButton> OnColorButtonPressed;
        
        [SerializeField]
        private bool defaultColor;
        [SerializeField]
        private Color color = Color.white;
        public Color Color => color;
        [SerializeField]
        private CanvasGroup selection;
        [SerializeField]
        private float fadeTime = 0.3f;
        [SerializeField]
        private Ease fadeEase = Ease.Flash;

        [SerializeField]
        private bool isRainbowColor = false;
        public bool IsRainbowColor => isRainbowColor;

        private Tween selectionTween;
        
        private void Start()
        {
            OnColorButtonPressed += HandleAnyColorButtonPressed;
            if (defaultColor)
            {
                SelectColor();
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            OnColorButtonPressed -= HandleAnyColorButtonPressed;
        }

        private void HandleAnyColorButtonPressed(ColorButton btn)
        {
            selectionTween?.Kill();
            selectionTween = selection.DOFade(btn == this ? 1 : 0, fadeTime).SetEase(fadeEase);
        }

        protected override void HandleButtonClick()
        {
            base.HandleButtonClick();
            SoundManager.PlayOneShot("colorPick");
            SelectColor();
        }

        private void SelectColor()
        {
            OnColorButtonPressed?.Invoke(this);
        }
    }
}
