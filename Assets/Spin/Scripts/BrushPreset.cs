﻿using UnityEngine;

namespace Spin.Scripts
{
	[CreateAssetMenu(fileName = "Brush Preset", menuName = "Brush Preset", order = 100)]
	public class BrushPreset : ScriptableObject
    {
	    #region Properties and variables

	    [SerializeField] private Color color = Color.white;
		public Color Color => color;

		[SerializeField] private Texture2D sourceTexture;
		public Texture2D SourceTexture
		{
			get => sourceTexture;
			set => sourceTexture = value;
		}

		#endregion

		public void SetColor(Color newColor)
		{
			color = newColor;
		}
    }
}