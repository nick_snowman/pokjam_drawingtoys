﻿using System;
using Common;
using DG.Tweening;
using UnityEngine;

namespace Spin.Scripts
{
    public class SpeedKnobe : MonoBehaviour
    {
        private const string SPIN_KEY = "spin";
        
        [System.Serializable]
        private class SpeedGroup
        {
            public float speed;
            public float localRotationZ;
            public float pitch;
        }
        
        [SerializeField]
        private SpeedGroup[] speedGroups;
        [SerializeField]
        private float speedChangeDuration = 0.5f;
        [SerializeField]
        private Ease speedChangeEase;
        
        private Plane dragPlane;
        private Vector3 offset;
        private Tween tween;
        private int currentSpeed;
        private bool soundPlaying;

        private void Start()
        {
            UpdateKnobe(true);
        }

        private void OnDestroy()
        {
            tween?.Kill();
            
            if(soundPlaying)
            {
                soundPlaying = false;
                SoundManager.StopLooping();
            }
        }

        public void OnMouseDown()
        {
            currentSpeed++;
            if (currentSpeed > speedGroups.Length - 1)
                currentSpeed = 0;
            UpdateKnobe();
            UpdateLoopSound();
        }

        private void UpdateKnobe(bool immediate = false)
        {
            SpeedGroup speedGroup = speedGroups[currentSpeed];
            tween?.Kill();
            tween = transform
                .DOLocalRotate(new Vector3(0, 0, speedGroup.localRotationZ), immediate ? 0 : speedChangeDuration)
                .SetEase(speedChangeEase);
            PaintingCanvas.Instance.ChangeSpeed(speedGroup.speed);
            SoundManager.PlayOneShot("spinSwitch");
        }
        
        private void UpdateLoopSound()
        {
            if (currentSpeed > 0)
            {
                if (soundPlaying)
                {
                    SpeedGroup speedGroup = speedGroups[currentSpeed];
                    SoundManager.TransitionLoopingPitch(speedGroup.pitch, 1);
                }
                else
                {
                    SoundManager.PlayLooping(SPIN_KEY);
                }

                soundPlaying = true;
            }
            else
            {
                if(soundPlaying)
                {
                    soundPlaying = false;
                    SoundManager.TransitionLoopingPitch(1, 0);
                    SoundManager.StopLooping();
                }
            }
        }
    }
}