using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spin.Scripts.UI;
public class ClearStamps : MonoBehaviour
{
    public GameObject stampHolder;

    // Start is called before the first frame update
    void Start()
    {
        ClearButton.OnClearButtonPressed += HandlerClearButtonPressed;   
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnDestroy()
    {
        ClearButton.OnClearButtonPressed -= HandlerClearButtonPressed;
    }
    private void HandlerClearButtonPressed(ClearButton clearButton)
    {
        foreach (Transform child in stampHolder.transform)
        {
            Destroy(child.gameObject);
        }
    }
    
}
