using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerStamp : MonoBehaviour
{
    public GameObject fingerStampPrefab;
    public Color paintColour;
    
    public GameObject paintBucket;
    
    public int stampRotationMagnitude = 25;

    public GameObject stampHolder;



    // Start is called before the first frame update
    void Start()
    {
      

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnMouseDown()
    {

        if (Input.GetMouseButton(0) && paintBucket.GetComponent<PaintSelection>().fingerDipped == true )
        {

            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            paintColour = paintBucket.GetComponent<PaintSelection>().selectedColor;

            //instantiate the finger print prefab at the mouse position
            GameObject stampedFinger = Instantiate(fingerStampPrefab, new Vector3(mousePos.x, mousePos.y, -1), Quaternion.Euler(new Vector3(0, 0, Random.Range(-stampRotationMagnitude, stampRotationMagnitude))));
            stampedFinger.GetComponent<SpriteRenderer>().color = paintColour;
            stampedFinger.transform.localScale *= Random.Range(0.7f,1);
            stampedFinger.transform.parent = stampHolder.transform;
            paintBucket.GetComponent<PaintSelection>().fingerDipped = false;
        }
    }
}
