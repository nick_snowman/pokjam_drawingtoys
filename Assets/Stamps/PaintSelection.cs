using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spin.Scripts.UI;

public class PaintSelection : MonoBehaviour
{

    SpriteRenderer m_spriteRenderer;
    public bool fingerDipped;

    public GameObject whitePaintOutline;

    public ParticleSystem splashParticle;

    private AudioSource m_AudioSource;
    public AudioClip[] dipSounds;

    public Color selectedColor;
    // Start is called before the first frame update
    void Start()
    {
        m_spriteRenderer = GetComponent<SpriteRenderer>();
        m_AudioSource = GetComponent<AudioSource>();

        ColorButton.OnColorButtonPressed += ChangeColour;
    }

    // Update is called once per frame
    private void OnDestroy()
    {
        ColorButton.OnColorButtonPressed -= ChangeColour;
    }
    private void ChangeColour(ColorButton colorBtn)
    {

        selectedColor = colorBtn.Color;
        if (selectedColor == new Color(1, 1, 1, 1))
        {
            whitePaintOutline.SetActive(true);

        }
        else
        {
            whitePaintOutline.SetActive(false);

        }

     
        m_spriteRenderer.color = colorBtn.Color;
    }

    private void OnMouseDown()
    {
        if (fingerDipped == false)
        {
            m_AudioSource.PlayOneShot(dipSounds[Random.Range(0, dipSounds.Length)]);
            fingerDipped = true;

            var mainPs = splashParticle.main;
            mainPs.startColor = selectedColor;

            splashParticle.gameObject.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, -1);
            splashParticle.Play();

        }
    }


}
