using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StampObject : MonoBehaviour
{
    [Header("Sprites")]
    public Sprite stationarySprite;
    public Sprite heldSprite;

    [Header("Prefabs")]
    public GameObject paint;
    public GameObject stamp;
    public ParticleSystem splashParticles;

    private float startPosX;
    private float startPosY;

    private Vector3 startScale;
    
    [Header("States")]
   [SerializeField] private bool isBeingHeld = false;
   [SerializeField] private bool hasPaint = false;

    public SpriteRenderer m_SpriteRenderer;

    public int stampRotationMagnitude = 25;
    public Color stampColour;

    public GameObject stampholder;

    [Header("Sounds")]

    public AudioClip[] dipSounds;
    public AudioClip[] dropSounds;
    public AudioClip[] liftSounds;
    public AudioClip[] stampingSounds;

    public AudioSource m_audioSource;
    void Start()
    {
        m_SpriteRenderer = transform.GetComponent<SpriteRenderer>();
        startScale = transform.localScale;
        m_audioSource = this.GetComponent<AudioSource>();
    }

    void Update()
    {
        if (isBeingHeld)
        {
            m_SpriteRenderer.sprite = heldSprite;

            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            this.gameObject.transform.localPosition = new Vector3(mousePos.x - startPosX, mousePos.y - startPosY, -1);
            
        }

    }
   


    private void OnMouseDown()
    {
        if (Input.GetMouseButton(0))
        {
            //play one of the pickup sounds
    
            m_audioSource.PlayOneShot(liftSounds[Random.Range(0, liftSounds.Length)]);


            m_SpriteRenderer.sortingOrder += 1;
            paint.GetComponent<SpriteRenderer>().sortingOrder += 1;

            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            startPosX = mousePos.x - this.transform.localPosition.x;
            startPosY = mousePos.y - this.transform.localPosition.y;

           
            
            isBeingHeld = true;
            this.transform.localScale *= 1.1f;
        }
    }

    private void OnMouseUp()
    {
        isBeingHeld = false;

        m_SpriteRenderer.sortingOrder -= 1;
        paint.GetComponent<SpriteRenderer>().sortingOrder -= 1;

        List<Collider2D> overlappingColliders = new List<Collider2D>();
        Collider2D m_collider = GetComponent<Collider2D>();

        ContactFilter2D filter = new ContactFilter2D().NoFilter();

        if (Physics2D.OverlapCollider(m_collider, filter, overlappingColliders) > 0)
        {
            Debug.Log(overlappingColliders);
            foreach (Collider2D overlappedcollider in overlappingColliders)
            {
                if (overlappedcollider .gameObject.tag == "PaintCan")
                {
                    Debug.Log("dropped in paint");

                    //play on of the splash sounds
                    m_audioSource.PlayOneShot(dipSounds[Random.Range(0, dipSounds.Length)]);

                    //play splash animation
                    var mainPs = splashParticles.main;
                    mainPs.startColor = overlappedcollider.GetComponentInChildren<PaintSelection>().selectedColor;
                    splashParticles.Play();

                    paint.gameObject.SetActive(true);
                    hasPaint = true;

                    //set the colour of the sprite to the colour of the selected paint
                    paint.gameObject.GetComponent<SpriteRenderer>().color = overlappedcollider.GetComponentInChildren<PaintSelection>().selectedColor;
                    stamp.gameObject.GetComponent<SpriteRenderer>().color = overlappedcollider.GetComponentInChildren<PaintSelection>().selectedColor;

                    m_SpriteRenderer.sprite = heldSprite;
                }

               else if (overlappedcollider.gameObject.tag == "StampCanvas" && hasPaint)
                {
                    Stamp();
                }
                else if (overlappedcollider.gameObject.tag == "StampCanvas" && hasPaint == false)
                {
                    //play one of the drop object sounds
                    m_SpriteRenderer.sprite = stationarySprite;
                    paint.gameObject.SetActive(false);
                }
                else if (overlappedcollider.gameObject.tag == "Stamp" && hasPaint == false)
                {
                    m_SpriteRenderer.sprite = stationarySprite;
                    paint.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            m_SpriteRenderer.sprite = stationarySprite;
            paint.gameObject.SetActive(false);
            //play one of the dropped sounds
            m_audioSource.PlayOneShot(dropSounds[Random.Range(0, dropSounds.Length)]);
        }
        transform.localScale = startScale;
        

    }
    public void Stamp()
    {

        //play one of the stamp sounds
        m_audioSource.PlayOneShot(stampingSounds[Random.Range(0, stampingSounds.Length)]);
        GameObject stampedStamp =  Instantiate(stamp, transform.position,Quaternion.Euler(new Vector3(0,0,Random.Range(-stampRotationMagnitude,stampRotationMagnitude))));
        stampedStamp.transform.parent = stampholder.transform;
        hasPaint = false;
        paint.gameObject.SetActive(false);
    }
    

    
}




