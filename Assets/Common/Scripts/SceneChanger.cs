using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Common
{
    /// <summary>
    /// Int enum corresponding to build index in Build Settings!
    /// </summary>
    public enum Toy
    {
        Menu = 0,
        ConnectTheDots = 1,
        Spin = 2,
        Stamps = 3
    }

    public class SceneChanger : MonoBehaviour
    {
        private const float SCREEN_FADE_TIME = 0.7f; // 0.5f;

        public static SceneChanger instance = null;
        private static CanvasGroup fadeCanvasGroup;

        private static Tween fadeTween = null;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this.gameObject);
                return;
            }
            
            DontDestroyOnLoad(this.gameObject);
            
            fadeCanvasGroup = GetComponentInChildren<CanvasGroup>();
            fadeCanvasGroup.alpha = 1;
            fadeCanvasGroup.blocksRaycasts = true;
            
            FadeOut();
            
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            FadeOut();
        }

        public static void ChangeScene(Toy toy)
        {
            fadeCanvasGroup.blocksRaycasts = true;

            KillActiveTween();
            fadeTween = fadeCanvasGroup.DOFade(1, SCREEN_FADE_TIME)
                .OnComplete(() => SceneManager.LoadScene((int) toy));
        }

        private static void FadeOut()
        {
            KillActiveTween();
            fadeTween = fadeCanvasGroup.DOFade(0, SCREEN_FADE_TIME)
                .From(1)
                .OnComplete(() => fadeCanvasGroup.blocksRaycasts = false);
        }

        private static void KillActiveTween()
        {
            if (fadeTween != null)
            {
                fadeTween.Kill();
            }
        }
    }
}