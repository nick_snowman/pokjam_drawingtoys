using UnityEngine;

namespace Common
{
   public class MenuManager : MonoBehaviour
   {
      private void OnEnable()
      {
         DraggableIcon.OnIconTapped += HandleIconTapped;
      }

      private void OnDisable()
      {
         DraggableIcon.OnIconTapped -= HandleIconTapped;
      }

      private void HandleIconTapped(Toy toy)
      {
         SceneChanger.ChangeScene(toy);
      }
   }
}