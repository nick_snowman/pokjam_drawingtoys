using UnityEngine;

public class ActiveOnPlatform : MonoBehaviour
{
    public enum Platform
    {
        Editor = 0,
        iOS,
        WebGL
    }

    [SerializeField][Tooltip("Will be active on this platform, otherwise will be disabled.")]
    private Platform platform;

    private void Awake()
    {
#if UNITY_EDITOR
        this.gameObject.SetActive(platform == Platform.Editor || platform == Platform.iOS);
#elif UNITY_IOS
        this.gameObject.SetActive(platform == Platform.iOS);
#elif UNITY_WEBGL
        this.gameObject.SetActive(platform == Platform.WebGL);
#else
        this.gameObject.SetActive(false);
#endif
    }
}
