using System.Collections;
using Audio;
using UnityEngine;

namespace Common
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundManager: MonoBehaviour
    {
        private const string RESOURCES_PATH = "SoundManager";
        private static SoundManager instance = null;

        [SerializeField]
        private AudioSource oneShotAudioSource;
        
        [SerializeField]
        private AudioSource loopAudioSource;

        [SerializeField]
        private SFXData sfxData;

        private Coroutine pitchCoroutine;

        private static SoundManager Instance
        {
            get
            {
                // if (_instance == null) _instance = FindObjectOfType<SoundManager>();

                if (instance == null)
                {
                    GameObject soundManagerPrefab = Instantiate(Resources.Load<GameObject>(RESOURCES_PATH));
                    instance = soundManagerPrefab.GetComponent<SoundManager>();
                } 
                    
                return instance;
            }
        }

        private void Awake()
        {
            sfxData.InitializeSFXData(sfxData);
        }

        private void OnDestroy()
        {
            instance = null;
        }

        private void OnValidate()
        {
            if (oneShotAudioSource != null && loopAudioSource != null)
            {
                oneShotAudioSource.loop = false;
                oneShotAudioSource.playOnAwake = false;
                oneShotAudioSource.spatialBlend = 0;
                
                loopAudioSource.loop = true;
                loopAudioSource.playOnAwake = false;
                loopAudioSource.spatialBlend = 0;
            }
        }

        public static void PlayOneShot(string soundEffectName)
        {
            Instance.oneShotAudioSource.PlayOneShot(GetClip(soundEffectName));
        }

        public static void PlayLooping(string soundEffectName)
        {
            Instance.loopAudioSource.clip = GetClip(soundEffectName);
            Instance.loopAudioSource.Play();
        }

        public static void StopLooping()
        {
            Instance.loopAudioSource.Stop();
        }

        public static void TransitionLoopingPitch(float pitch, float transitionTime)
        {
            if (Instance.pitchCoroutine != null)
            {
                Instance.StopCoroutine(Instance.pitchCoroutine);
            }

            Instance.pitchCoroutine = Instance.StartCoroutine(Instance.PitchTransitionRoutine(pitch, transitionTime));
        }

        private IEnumerator PitchTransitionRoutine(float pitch, float transitionTime)
        {
            float elapsed = 0f;
            float startingPitch = Instance.loopAudioSource.pitch;
            float goalPitch = Mathf.Clamp(pitch, -3f, 3f);

            while (elapsed < transitionTime)
            {
                float t = (elapsed / transitionTime);
                t *= t;

                Instance.loopAudioSource.pitch = Mathf.Lerp(startingPitch, goalPitch, t);

                elapsed += Time.deltaTime;
                yield return null;
            }

            Instance.loopAudioSource.pitch = goalPitch;
        }

        private static AudioClip GetClip(string soundEffectName)
        {
            return Instance.sfxData.GetClip(soundEffectName);
        }
    }
}
