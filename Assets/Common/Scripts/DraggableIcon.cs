using System.Collections;
using UnityEngine;

namespace Common
{
    [RequireComponent(typeof(Rigidbody2D), typeof(Animation))]
    public class DraggableIcon : MonoBehaviour
    {
        private const float DRAG_SPEED = 10f;
        private const float BUTTON_HOLD_TIME_THRESHOLD = 0.1f;
        private const float JIGGLE_OFFSET_INCREMENT = 0.1667f;

        public static event System.Action<Toy> OnIconTapped;
        
        private static bool isFirstJiggler = true;
        private static float jiggleOffset = 0f;

        [SerializeField]
        private Toy toy;

        private Animation attachedAnimation;
        private Rigidbody2D rb2d;
        private Camera mainCameraRef;
        private bool iconTapped;
        private bool isDragging;
        private Vector2 CurrentMousePosition => mainCameraRef.ScreenToWorldPoint(Input.mousePosition);

        private void OnEnable()
        {
            DraggableIcon.OnIconTapped += HandleIconTapped;

            StartCoroutine(JiggleRoutine());
        }

        private void OnDisable()
        {
            DraggableIcon.OnIconTapped -= HandleIconTapped;
        }

        private IEnumerator JiggleRoutine()
        {
            if (isFirstJiggler)
            {
                isFirstJiggler = false;
                yield return new WaitForSeconds(jiggleOffset);
            }
            else
            {
                jiggleOffset += JIGGLE_OFFSET_INCREMENT;
                yield return new WaitForSeconds(jiggleOffset);
            }

            if (attachedAnimation == null)
            {
                attachedAnimation = GetComponent<Animation>();
            }

            attachedAnimation.clip.wrapMode = WrapMode.Loop;
            attachedAnimation.Play();
        }

        // Disable all icons when one has been tapped
        private void HandleIconTapped(Toy toy)
        {
            iconTapped = true;
        }

        private void Start()
        {
            mainCameraRef = Camera.main;
            rb2d = GetComponent<Rigidbody2D>();
        }

        private void OnMouseOver()
        {
            if (!iconTapped && !isDragging && Input.GetMouseButtonDown(0))
            {
                isDragging = true;
                StartCoroutine(DragRoutine());
            }
        }

        private IEnumerator DragRoutine()
        {
            float deltaTime = Time.deltaTime;
            float timeHeld = 0;

            while (Input.GetMouseButton(0))
            {
                rb2d.AddForce((CurrentMousePosition - (Vector2) transform.position) * (deltaTime * DRAG_SPEED));

                yield return null;
                deltaTime = Time.deltaTime;
                timeHeld += deltaTime;
            }

            isDragging = false;

            if (timeHeld <= BUTTON_HOLD_TIME_THRESHOLD)
            {
                OnIconTapped?.Invoke(toy);
                SoundManager.PlayOneShot("colorPick");
            }
        }
    }
}