using Common;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour
{
    public void ReturnToMenu()
    {
        if (SceneChanger.instance != null)
        {
            SceneChanger.ChangeScene(Toy.Menu);
        }
        else
        {
            Debug.Log("SceneChanger not found!");
            SceneManager.LoadScene((int) Toy.Menu);
        }
    }
}
