using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Audio
{
    [Serializable]
    public class SoundEffect
    {
        private enum PlaybackMode
        {
            Random, // Works for a single sound too
            RoundRobin,
            Loop
        }

        [SerializeField] private AudioClip[] clips;

        [SerializeField] private PlaybackMode playbackMode;

        private uint rrIndex;

        public AudioClip Clip
        {
            get
            {
                return playbackMode switch
                {
                    PlaybackMode.Random => clips[Random.Range(0, clips.Length)],
                    PlaybackMode.RoundRobin => clips[rrIndex++ % clips.Length],
                    PlaybackMode.Loop => clips[0],
                    _ => throw new ArgumentOutOfRangeException()
                };
            }
        }
    }
}