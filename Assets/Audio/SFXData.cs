using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Audio
{
    [CreateAssetMenu]
    public class SFXData : ScriptableObject
    {
        private Dictionary<string, SoundEffect> soundEffects;
        
        public void InitializeSFXData(SFXData sfxDataInstance)
        {
            soundEffects = new Dictionary<string, SoundEffect>();

            Type type = typeof(SFXData);
            BindingFlags allFlags = (BindingFlags) (-1);
            
            foreach (FieldInfo fieldInfo in type.GetFields(allFlags))
            {
                if (fieldInfo.FieldType == typeof(SoundEffect))
                {
                    soundEffects.Add(fieldInfo.Name, (SoundEffect) fieldInfo.GetValue(sfxDataInstance));
                }
            }
        }

        public AudioClip GetClip(string soundEffectName)
        {
            return soundEffects[soundEffectName].Clip;
        }
        
        [Header("General")]
        [SerializeField] private SoundEffect clearCanvas;
        [SerializeField] private SoundEffect colorPick;

        [Header("Connect the Dots")]
        [SerializeField] private SoundEffect colorFillRR;
        [SerializeField] private SoundEffect colorFillBlue;
        [SerializeField] private SoundEffect colorFillGreen;
        [SerializeField] private SoundEffect colorFillOrange;
        [SerializeField] private SoundEffect colorFillRed;
        [SerializeField] private SoundEffect colorFillYellow;

        [Header("Spin")]
        [SerializeField] private SoundEffect spin;
        [SerializeField] private SoundEffect spinSwitch;
        [SerializeField] private SoundEffect brushSelect;

        [Header("Stamps")]
        [SerializeField] private SoundEffect stamps;
    }
}